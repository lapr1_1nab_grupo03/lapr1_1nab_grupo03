
package lapr1;

import static lapr1.MetodosComuns.ValorMaxMatriz;
import static lapr1.MetodosComuns.somaColunaMatriz;
import static lapr1.MetodosComuns.vecNorm;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;


public class CalcAHP {

    /*
    ################################################
    ################ CALCULO APROXIMADO #################
    ################################################
    */
    
    //calcula vetor proprio
    /**
     * 
     * @param matriz - Matriz normalizada
     * @param nEle - Numero de elementos da matriz 
     * @return - Vetor proprio da matriz
     */
    public static double[] VetorProprioMan(double[][] matriz, int nEle) {

        double[] vetorproprio = new double[nEle];

        double soma = 0;
        for (int i = 0; i < nEle; i++) {
            for (int j = 0; j < nEle; j++) {
                soma = soma + matriz[i][j];
            }
            vetorproprio[i] = soma / nEle;
            soma = 0;
        }

        return vetorproprio;
    }

    //calcula valor proprio de uma matriz
    /**
     * 
     * @param matriz - Matriz normalizada
     * @param vetorproprio - Vetor proprio
     * @param nEle - Numero de elementos
     * @return - Valor proprio
     */
    public static double ValorProprioMan(double[][] matriz, double[] vetorproprio, int nEle) {

        double[] AX = new double[nEle];
        double soma = 0, lmax = 0;
        int indicevp = 0, tamanho = nEle;

        for (int i = 0; i < tamanho; i++) {
            for (int j = 0; j < tamanho; j++) {

                soma = soma + (matriz[i][j] * vetorproprio[indicevp]);
                indicevp++;
            }
            AX[i] = soma;
            soma = 0;
            indicevp = 0;
        }
        /* Agora calculo o valor proprio */
        indicevp = 0;
        for (int i = 0; i < tamanho; i++) {
            lmax = lmax + (AX[i] / vetorproprio[i]);
        }
        lmax = lmax / tamanho;

        
        return lmax;

    }

    /*
     ################################################
     ################ CALCULO EXATO #################
     ################################################
     */
    //devolve vector proprio de uma determinada matriz
    /**
     * 
     * @param matriz - Matriz de decisao
     * @param lambdasMax - Vetor proprio da matriz
     * @param posM - Posicao da matriz
     * @return - Vetor proprio
     */
    public static double[] calculaVetorProp(double[][] matriz, double[] lamdasMax, int posM) {

        Matrix a = new Basic2DMatrix(matriz);

        //Obtem valores e vetores proprios fazendo "Eigen Decomposition"
        EigenDecompositor eigenD = new EigenDecompositor(a);
        Matrix[] mattD = eigenD.decompose();

        // converte objeto Matrix (duas matrizes)  para array Java        
        double matA[][] = mattD[0].toDenseMatrix().toArray();
        double matB[][] = mattD[1].toDenseMatrix().toArray();

        //calcula lambda max e preenche vector
        double max = ValorMaxMatriz(matB);
        lamdasMax[posM] = max;

        //determina a coluna do valor maximo
        int coluna = 0;
        for (int i = 0; i < matB.length; i++) {
            for (int j = 0; j < matB[i].length; j++) {
                if (matB[i][j] == max) {
                    coluna = j;
                }
            }
        }

        double somaColuna = somaColunaMatriz(matA, coluna);

        double[] vec = vecNorm(matA, coluna, somaColuna);

        return vec;
    }

}
