package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import static lapr1.Lapr1.theMatrix;
import static lapr1.MetodosComuns.ir;

/*
    !!!!!!!!!!preenchimento da matriz(slots/gavetas)AHP!!!!!!!!!!!
    matrizes do ficheiro:   [0-(nCrit+1)[
    matrizes normalizadas:   [(nCrit+1)-((nCrit +1)*2)[
    vetores proprios:   [((nCrit +1)*2)-((nCrit +1)*3)[
    valores pproprios(lambda max):   [((nCrit +1)*3)]
    rc:    [(((nCrit +1)*3)+1)]
    vetor composto:       [(((nCrit +1)*3)+2)]
    escolha:   [(((nCrit +1)*3)+3)]
 */
public class Escrita_txt {

    /**
     * metodo de escrita para AHP
     *
     * @param fileOut - ficheiro de saida
     * @param crit - cabecalho criterios
     * @param alt - cabecalho alternativas
     * @param nCrit - numero de criterios
     * @param nAlt - numero de alternativas
     * @param critEliminados - vetor com criterios eliminados
     * @throws FileNotFoundException
     */
    public static void escritaAhp(String fileOut, String[] crit, String[] alt, int nCrit, int nAlt, String[] critEliminados) throws FileNotFoundException {

        int slot = nCrit + 1;

        Formatter out = new Formatter(new File(fileOut));
        //output de titulo
        System.out.printf("%n%35s%n", "--METODO AHP--");
        out.format("%n%35s%n", "--METODO AHP--");
        //saida para consola e ficheiro dos criterios eliminados
        outCritEliminados(critEliminados, nCrit, out);
        //saida de inconsistencia
        outConsistencia(out, nCrit, crit);
        //saida para consola e ficheiro das matrizes normalizadas
        outCabecalhosAhp(2, out);
        outMatInMatNorm(crit, alt, nCrit, nAlt, out, slot, slot * 2);
        //saida para ficheiro de vetores e valores proprios,rc's e ir's
        outCabecalhosAhp(3, out);
        outVetValRcIr(crit, alt, nCrit, nAlt, out, slot * 2, slot * 3);
        //saida para consola e ficheiro das matrizes entrada
        outCabecalhosAhp(1, out);
        outMatInMatNorm(crit, alt, nCrit, nAlt, out, 0, slot);
        //saida para consola e ficheiro de vetor composto e escolha preferida
        outCabecalhosAhp(4, out);
        outCompPref(nCrit, alt, out, ((slot * 3) + 2));

        out.close();

    }

    /**
     * verifica se existem matrizes inconsistentes se existirem escreve quais
     *
     * @param out
     * @param nCrit - nuemro de criterios
     * @param crit - vetor cabecalho criterios
     */
    public static void outConsistencia(Formatter out, int nCrit, String[] crit) {
        int count = 0;
        for (int i = 1; i < nCrit; i++) {
            if (theMatrix[i][99][99] != 0 && count == 0) {
                System.out.printf("%n%s%n%s%n", "As seguintes matrizes foram dadas como inconsistentes", "de acordo com a consistencia definida:");
                out.format("%n%s%n%s%n", "As seguintes matrizes foram dadas como inconsistentes", "de acordo com a consistencia definida:");
                int pos = (int) theMatrix[i][99][99];
                System.out.printf("->%s%n", crit[pos]);
                out.format("->%s%n", crit[pos]);
                count++;
            } else {
                if(theMatrix[i][99][99] != 0){
                int pos = (int) theMatrix[i][99][99];
                System.out.printf("->%s%n", crit[pos]);
                out.format("->%s%n", crit[pos]);
                }
            }

        }
    }

    /**
     * metodo para output dos criterios eliminados, se existirem
     *
     * @param critEliminados - vetor com criterios eliminados
     * @param nCrit - numero de criterios
     * @param out
     */
    public static void outCritEliminados(String[] critEliminados, int nCrit, Formatter out) {

        if (critEliminados[0].charAt(0) != '0') {
            System.out.printf("%n%s%n", "CRITERIOS ELIMINADOS");
            out.format("%n%s%n", "CRITERIOS ELIMINADOS");
            for (int i = 1; i < critEliminados.length - nCrit; i++) {

                System.out.printf("->%s%n", critEliminados[i]);
                out.format("->%s%n", critEliminados[i]);
            }
        }
    }

    /**
     * metodo para output de matrizes de entrada e normalizadas
     *
     * @param crit - vetor cabecalho criterios
     * @param alt - vetor cabecalho alternativas
     * @param nCrit - numero de criterios
     * @param nAlt - numero de alternativas
     * @param out
     * @param slotI - slot inicial (consultar legenda topo)
     * @param slotF - slot final (consultar legenda topo)
     */
    public static void outMatInMatNorm(String[] crit, String[] alt, int nCrit, int nAlt, Formatter out, int slotI, int slotF) {
        int aux = slotI;
        for (int k = slotI; k < slotF; k++) {

            if (k == slotI) {//matriz de criterios
                for (int x = 0; x < nCrit + 1; x++) {

                    /*se forem matrizes de entrada, saiem para file e consola.
                    senao, sao matrizes normalizadas e saiem so para file
                     */
                    if (slotI < nCrit + 1) {
                        System.out.printf("%s  ", crit[x]);
                        out.format("%s  ", crit[x]);
                    } else {
                        out.format("%s  ", crit[x]);
                    }
                }
                if (aux < nCrit + 1) {
                    LineBreakConsole(out);
                    LineBreakFile(out);
                } else {
                    LineBreakFile(out);
                }

                for (int i = 0; i < nCrit; i++) {

                    for (int j = 0; j < nCrit; j++) {
                        if (slotI < nCrit + 1) {
                            System.out.printf("%.2f  ", theMatrix[i][j][k]);
                            out.format("%.2f  ", theMatrix[i][j][k]);
                        } else {
                            out.format("%.2f  ", theMatrix[i][j][k]);
                        }
                    }
                    if (aux < nCrit + 1) {
                        LineBreakConsole(out);
                        LineBreakFile(out);
                    } else {
                        LineBreakFile(out);
                    }
                }
                if (aux < nCrit + 1) {
                    LineBreakConsole(out);
                    LineBreakFile(out);
                } else {
                    LineBreakFile(out);
                }
            } else {//matrizes de alternativas
                if (slotI < nCrit + 1) {//se forem matrizes de entrada, saiem para file e consola
                    System.out.printf("%s%s ", "mcp_", crit[k - aux]);
                    out.format("%s%s ", "mcp_", crit[k - aux]);
                } else {//senao, sao matrizes normalizada e saiem so para file
                    out.format("%s%s ", "mcp_", crit[k - aux]);
                }
                for (int x = 1; x < alt.length; x++) {
                    if (slotI < nCrit + 1) {
                        System.out.printf("%s ", alt[x]);
                        out.format("%s ", alt[x]);
                    } else {
                        out.format("%s ", alt[x]);
                    }
                }
                if (aux < nCrit + 1) {
                    LineBreakConsole(out);
                    LineBreakFile(out);
                } else {
                    LineBreakFile(out);
                }

                for (int i = 0; i < nAlt; i++) {

                    for (int j = 0; j < nAlt; j++) {
                        if (slotI < nCrit + 1) {
                            System.out.printf(" %.2f ", theMatrix[i][j][k]);
                            out.format(" %.2f ", theMatrix[i][j][k]);
                        } else {
                            out.format(" %.2f ", theMatrix[i][j][k]);

                        }

                    }

                    if (aux < nCrit + 1) {
                        LineBreakConsole(out);
                        LineBreakFile(out);
                    } else {
                        LineBreakFile(out);
                    }
                }
                if (aux < nCrit + 1) {
                    LineBreakConsole(out);
                    LineBreakFile(out);
                } else {
                    LineBreakFile(out);
                }
            }
        }
        if (aux < nCrit + 1) {
            LineBreakConsole(out);
            LineBreakFile(out);
        } else {
            LineBreakFile(out);
        }
    }

    //quebra de linha para ficheiro
    public static void LineBreakFile(Formatter out) {

        out.format("%n");
    }

    //quebra de linha para consola
    public static void LineBreakConsole(Formatter out) {
        System.out.printf("%n");

    }

    /**
     * metodo com os cabecalhos para output AHP
     *
     * @param num - numero do cabecalho a utilizar
     * @param out
     */
    public static void outCabecalhosAhp(int num, Formatter out) {
        String tracos = "-----------------------------------------------------";
        switch (num) {

            case 1:
                System.out.printf("%n%s%n", tracos);
                System.out.printf("%s", "MATRIZES DE ENTRADA");
                System.out.printf("%n%s%n", tracos);
                out.format("%n%40s%n", tracos);
                out.format("%s", "MATRIZES DE ENTRADA");
                out.format("%n%40s%n", tracos);
                break;
            case 2:
                out.format("%n%40s%n", tracos);
                out.format("%s", "MATRIZES NORMALIZADAS");
                out.format("%n%40s%n", tracos);
                break;
            case 3:
                out.format("%n%40s%n", tracos);
                out.format("%s%20s%8s%8s", "|VETORES PROPRIOS|", "|VALORES PROPRIOS|", "|RC's|", "|IR's|");
                out.format("%n%40s%n", tracos);
                break;

            case 4:
                System.out.printf("%n%s%n", tracos);
                System.out.printf("%s", "VETOR COMPOSTO E OPÇAO PREFERIDA");
                System.out.printf("%n%s%n", tracos);
                out.format("%n%40s%n", tracos);
                out.format("%s", "VETOR COMPOSTO E OPÇAO PREFERIDA");
                out.format("%n%40s%n", tracos);
                break;

        }

    }

    /**
     * metodo de output para ficheiro de vetores/valores proprios, rc e ir(estes
     * 2 utilizam metodo comum dentro deste)
     *
     * @param crit - vetor cabecalho criterios
     * @param alt - vetor cabecalho alternativas
     * @param nCrit - numero de criterios
     * @param nAlt - numero de alternativas
     * @param out
     * @param slotI - slot inicial (consultar legenda topo)
     * @param slotF - slot final (consultar legenda topo)
     */
    public static void outVetValRcIr(String[] crit, String[] alt, int nCrit, int nAlt, Formatter out, int slotI, int slotF) {
        int count = 0;
        int aux = slotI;
        while (slotI < slotF) {

            if (slotI == aux) {
                out.format("%s%n", crit[count]);
                out.format("%s%16s%10s%10s%n%n", "vetor proprio", "valor proprio", "RC", "IR");

                for (int j = 0; j < nCrit; j++) {
                    if (j == 0) {
                        out.format("%.2f", theMatrix[j][0][slotI]);
                        //metodo de saida para ficheiro de rc e ir
                        outValRc(j, out, slotF);
                        outValRc(j, out, slotF + 1);
                        out.format("%7s", "");
                        out.format("%.2f", ir[nCrit - 1]);
                        LineBreakFile(out);
                    } else {
                        out.format("%.2f%n", theMatrix[j][0][slotI]);
                    }
                }

                LineBreakFile(out);
                LineBreakFile(out);
                count++;
                slotI++;
            } else {

                for (int i = 0; i < nCrit; i++) {
                    out.format("mcp_%s%n", crit[count]);
                    out.format("%s%16s%10s%10s%n%n", "vetor proprio", "valor proprio", "RC", "IR");
                    for (int j = 0; j < nAlt; j++) {

                        if (j == 0) {
                            out.format("%.2f", theMatrix[j][i][slotI]);
                            //metodo de saida para ficheiro de rc e ir
                            outValRc(i + 1, out, slotF);
                            outValRc(i + 1, out, slotF + 1);
                            out.format("%7s", "");
                            out.format("%.2f", ir[nAlt - 1]);

                            LineBreakFile(out);
                        } else {
                            out.format("%.2f%n", theMatrix[j][i][slotI]);
                        }
                    }
                    count++;
                    slotI++;

                    LineBreakFile(out);
                    LineBreakFile(out);

                }

            }

        }
        LineBreakFile(out);
        LineBreakFile(out);
    }

    /**
     * metodo de saida para ficheiro de rc e ir
     *
     * @param i - linha
     * @param out
     * @param slotF - slot onde vai buscar valor (consultar legenda topo)
     */
    public static void outValRc(int i, Formatter out, int slotF) {

        out.format("%14s", "");
        out.format("%.2f", theMatrix[i][0][slotF]);

    }

    /**
     * metodo de saida para ficheiro e consola do vetor composto e escolha
     * preferida
     *
     * @param nCrit - numero de criterios
     * @param alt - vetor cabecalho alternativas
     * @param out
     * @param slotF - slot onde vai buscar valor (consultar legenda topo)
     */
    public static void outCompPref(int nCrit, String[] alt, Formatter out, int slotF) {
        int pos = ((int) theMatrix[0][0][slotF + 1]);

        for (int i = 0; i < alt.length - 1; i++) {
            if (i == pos - 1) {
                System.out.printf("%.2f ", theMatrix[i][0][slotF]);
                out.format("%.2f", theMatrix[i][0][slotF]);

                System.out.printf("  --------%s", "Melhor alternativa:");
                out.format("  --------%s", "Melhor alternativa:");

                System.out.printf("|%s|%n", alt[pos]);
                out.format("  ||%s||  %n", alt[pos]);
            } else {

                System.out.printf("%.2f%n", theMatrix[i][0][slotF]);
                out.format("%.2f%n", theMatrix[i][0][slotF]);
            }
        }
    }

    /*   
    !!!!!!!!!!preenchimento da matriz(slots/gavetas)TOPSIS!!!!!!!!!!!
    vetor de pesos:   [0]
    matriz decisao:   [1]
    matriz decisao normalizada:   [2]
    matriz decisao normalizada pesada:   [3]
    solucoes ideais:    [4] 2 vetores
    distancia alternativas:       [5] 2 vetores
    vetor proximidades:   [6] 1 vetor
    alternativa escolhida: [7]
     */
    /**
     * metodo de escrita para TOPSIS
     *
     * @param fileOut - ficheiro de saida
     * @param vecCrit - vetor de criterios
     * @param vecAlt - vetor de alternativas
     * @throws FileNotFoundException
     */
    public static void escritaTopsis(String fileOut, String[] vecCrit, String[] vecAlt) throws FileNotFoundException {

        Formatter out = new Formatter(new File(fileOut));
        //output de titulo
        System.out.printf("%n%35s%n", "--METODO TOPSIS--");
        out.format("%n%35s%n", "--METODO TOPSIS--");
        //outputs de dados de entrada
        outCabecalhosTopsis(1, out);
        outVecCabecalhos(vecCrit, out, 0, "vec_pesos");
        outputVecTopsis(vecCrit, vecCrit, 0, out);
        System.out.printf("%n%n%s%n", "md_crt_alt");
        out.format("%n%n%s%n", "md_crt_alt");
        outVecCabecalhos(vecCrit, out, 0, "crt");
        outVecCabecalhos(vecAlt, out, 0, "alt");
        outputMatTopsis(vecCrit, vecAlt, 1, out);
        //output de matriz normalizada
        outCabecalhosTopsis(2, out);
        outputMatTopsis(vecCrit, vecAlt, 2, out);
        //output de matriz normalizada pesada
        outCabecalhosTopsis(3, out);
        outputMatTopsis(vecCrit, vecAlt, 3, out);
        //output de solucoes ideais
        outCabecalhosTopsis(4, out);
        outputVecTopsis(vecAlt, vecCrit, 4, out);
        //output de distancias alternativas
        outCabecalhosTopsis(5, out);
        outputVecTopsis(vecAlt, vecCrit, 5, out);
        //output de vetor proximidades e alternativa ideal
        outCabecalhosTopsis(6, out);
        outputVecTopsis(vecAlt, vecCrit, 6, out);
        out.close();

    }

    /**
     * metodo com os cabecalhos para output TOPSIS
     *
     * @param num - numero do cabecalho a utilizar
     * @param out
     */
    public static void outCabecalhosTopsis(int num, Formatter out) {
        String tracos = "-----------------------------------------------------";
        switch (num) {

            case 1:
                System.out.printf("%n%s%n", tracos);
                System.out.printf("%s%n%s", "DADOS DE ENTRADA", "VETOR DE PESOS E MATRIZ DECISAO");
                System.out.printf("%n%s%n", tracos);
                out.format("%n%40s%n", tracos);
                out.format("%s%n%s", "DADOS DE ENTRADA", "VETOR DE PESOS E MATRIZ DECISAO");
                out.format("%n%40s%n", tracos);
                break;

            case 2:
                out.format("%n%40s%n", tracos);
                out.format("%s", "MATRIZ NORMALIZADA");
                out.format("%n%40s%n", tracos);
                break;

            case 3:
                System.out.printf("%n%40s%n", tracos);
                System.out.printf("%s", "MATRIZ NORMALIZADA PESADA");
                System.out.printf("%n%40s%n", tracos);
                out.format("%n%40s%n", tracos);
                out.format("%s", "MATRIZ NORMALIZADA PESADA");
                out.format("%n%40s%n", tracos);
                break;
            case 4:
                out.format("%n%40s%n", tracos);
                out.format("%s", "SOLUÇOES IDEAIS");
                out.format("%n%40s%n", tracos);
                out.format("%s%10s%n", "Positiva", "Negativa");
                break;
            case 5:
                out.format("%n%40s%n", tracos);
                out.format("%s", "DISTANCIAS ALTERNATIVAS");
                out.format("%n%40s%n", tracos);
                out.format("%s%10s%n", "Positiva", "Negativa");
                break;
            case 6:
                System.out.printf("%n%40s%n", tracos);
                System.out.printf("%s", "VETOR PROXIMIDADES E MELHOR ALTERNATIVA");
                System.out.printf("%n%40s%n", tracos);
                out.format("%n%40s%n", tracos);
                out.format("%s", "VETOR PROXIMIDADES E MELHOR ALTERNATIVA");
                out.format("%n%40s%n", tracos);
                break;

        }

    }

    /**
     * output de cabecalhos adicionais (pesos,crt,alt)
     *
     * @param vetor - vetor com strings
     * @param out
     * @param num - caso seja so para ficheiro, ou para consola e ficheiro (so
     * esta a ser utilizado para o ultimo)
     * @param txt
     */
    public static void outVecCabecalhos(String[] vetor, Formatter out, int num, String txt) {
        if (num == 1) {
            System.out.printf("%s  ", txt);
            for (int i = 0; i < vetor.length; i++) {

                out.format("%s  ", vetor[i]);

            }
            LineBreakConsole(out);
        } else {
            System.out.printf("%s  ", txt);
            out.format("%s  ", txt);
            for (int i = 0; i < vetor.length; i++) {
                System.out.printf("%s  ", vetor[i]);
                out.format("%s  ", vetor[i]);

            }
            LineBreakConsole(out);
            LineBreakFile(out);
        }
    }
    
    /**
     * output para matrizes topsis, conforme o slot sai so para ficheiro ou para consola e ficheiro
     * @param vecCrit - vetor de criterios
     * @param vecAlt - vetor de alternativas
     * @param slot - slot da matriz principal (consultar legenda no inicio topsis)
     * @param out 
     */
    public static void outputMatTopsis(String[] vecCrit, String[] vecAlt, int slot, Formatter out) {

        for (int i = 0; i < vecAlt.length; i++) {

            for (int j = 0; j < vecCrit.length; j++) {
                if (slot == 2) {

                    out.format("%.2f  ", theMatrix[i][j][slot]);
                } else {
                    System.out.printf("%.2f  ", theMatrix[i][j][slot]);
                    out.format("%.2f  ", theMatrix[i][j][slot]);
                }
            }
            if (slot == 2) {

                LineBreakFile(out);
            } else {
                LineBreakConsole(out);
                LineBreakFile(out);

            }
        }

    }
    
    /**
     * output para vetores topsis, conforme o slot sai so para ficheiro ou para consola e ficheiro
     * @param vecAlt - vetor de alternativas
     * @param vecCrit - vetor de criterios
     * @param slot - slot da matriz principal (consultar legenda no inicio topsis)
     * @param out 
     */
    public static void outputVecTopsis(String[] vecAlt, String[] vecCrit, int slot, Formatter out) {
        if (slot == 4) {
            for (int j = 0; j < vecCrit.length; j++) {

                out.format("  %.2f%6s%.2f", theMatrix[j][0][slot], "", theMatrix[j][1][slot]);
                out.format("%20s%n", vecCrit[j]);
            }
        } else {
            for (int j = 0; j < vecAlt.length; j++) {
                if (slot == 5) {

                    out.format("  %.2f%6s%.2f", theMatrix[j][0][slot], "", theMatrix[j][1][slot]);
                    out.format("%20s%n", vecAlt[j]);
                } else {
                    if (slot == 0) {
                        System.out.printf("%.2f  ", theMatrix[j][0][slot]);
                        out.format("%.2f  ", theMatrix[j][0][slot]);
                    } else {
                        //verifica qual o indice da melhor alternativa, para escrever á frente desse valor no vetor de proximidades
                        if (j == theMatrix[0][0][7]) {

                            System.out.printf("%.2f  ", theMatrix[j][0][slot]);
                            out.format("%.2f  ", theMatrix[j][0][slot]);

                            System.out.printf("  --------%s", "Melhor alternativa:");
                            out.format("  --------%s", "Melhor alternativa:");

                            System.out.printf("|%s|%n", vecAlt[j]);
                            out.format("  ||%s||  %n", vecAlt[j]);
                        } else {
                            System.out.printf("%.2f%n", theMatrix[j][0][slot]);
                            out.format("%.2f%n", theMatrix[j][0][slot]);
                        }
                    }
                }
            }
        }
    }
}
