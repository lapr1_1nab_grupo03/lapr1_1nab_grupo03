
package lapr1;

public class MetodosComuns {

    public static double[] ir = {0, 0, 0.58, 0.9, 1.12, 1.24};
    
    public static void calculoTOPSIS(double[][][]theMatrix, int nAlt, int nCrit, int custos){
        
        
        //Retira matriz de decisão da matriz cubica
        double[][]matAux=criaMatAux(theMatrix, nAlt, nCrit, 1);
        
        //Normaliza matriz
        double[][]matAuxNorm=CalcTOPSIS.normalizador(matAux, nAlt, nCrit);
        
        //Insere matriz normalizada na cubica na slot 2
        Leitura_txt.introduzirMatrizesCalc(theMatrix, 2, matAuxNorm, nAlt, nCrit);
        
        //Retira vetor de pesos da matriz cubica
        double []vecPesos=criaVecAux(theMatrix, nCrit, 0, 0);
        
        //Pesa matriz normalizada
        double[][]mp=CalcTOPSIS.matriznormpesada(matAuxNorm, vecPesos, nCrit, nAlt);
        
        //Insere matriz normalizada pesada na slot 3 da matriz cubica
        Leitura_txt.introduzirMatrizesCalc(theMatrix, 3, mp, nAlt, nCrit);
        
        //Calcular vectores ideais positivo e negativo e insere na matriz cubica na slot 4
        System.out.println("alt "+nAlt+" crit "+nCrit);
        double[]vecIdealPos=CalcTOPSIS.idealpositivo(mp, nCrit, nAlt, custos);
        double[]vecIdealNeg=CalcTOPSIS.idealnegativo(mp, nCrit, nAlt, custos);
        
        Leitura_txt.introduzirVetoresUnicos(theMatrix, 4, vecIdealPos, nCrit, 0);
        Leitura_txt.introduzirVetoresUnicos(theMatrix, 4, vecIdealNeg, nCrit, 1);
        
        //Calcula Vetor de distância euclidiana relativamente à solução ideal positiva e negativa.
        //Insere na matrix cubica na slot 5
        
        double[]distPos=CalcTOPSIS.distanciaalternativa(mp, nAlt, nCrit, vecIdealPos);    
        double[]distNeg=CalcTOPSIS.distanciaalternativa(mp, nAlt, nCrit, vecIdealNeg);
        
        Leitura_txt.introduzirVetoresUnicos(theMatrix, 5, distPos, nAlt, 0);
        Leitura_txt.introduzirVetoresUnicos(theMatrix, 5, distNeg, nAlt, 1);
        
        //Calcula vetor de proximidade e insere na matriz cubica na slot 6
        
        double[]prox=CalcTOPSIS.proximidade(distPos, distNeg, nAlt);
        
        Leitura_txt.introduzirVetoresUnicos(theMatrix, 6, prox, nAlt, 0);
        
        //Identifica alternativa ideal
        
        double valIdeal=ValorMaxVector(prox);
        
        //Indentifica a posição da alternativa ideal no vetor e guarda o indice
        int index = 0;
        for (int i = 0; i < prox.length; i++) {
            if (prox[i] == valIdeal) {
                index = i;
            }
        }
        
        theMatrix[0][0][7]=index;
        
        
        
        
    }

    public static void calculoAhp(double[][][] theMatrix, int nCrit, int nAlt, String[] crit, String[] alt, int op, String limite) {

        int numMat = nCrit + 1, w=0;

        //cria Matriz para guardar vetores proprios mediante o numero de criterios (colunas)
        // e mediante o numero de alternativas (linhas)
        double[][] matVecProprios = new double[nAlt][nCrit];

        //cria vector valores proprios da matriz dos criterios
        double[] vecPropCriterios = new double[nCrit];

        //cria vetor para guardar lamdas Max
        double[] lamdasMax = new double[numMat];

        double[] vecRC = new double[numMat];

        for (int z = 0; z < numMat; z++) {
            //conta numero de coluna ou linhas da matriz na pozicao z da matriz cubica
            int numEle = numElementos(theMatrix, z);

            //cria uma matriz da Mat Cubica para o calculo do seu vector proprio
            //mediante a sua posicao na cubica
            double[][] matAux = criaMatAux(theMatrix, numEle, numEle, z);

            //calcula vetor proprio de cada matriz
            // e separa o vetor de criterios dos restantes que ficam numa matriz
            //guarda os lamdas max no respetivo vetor (a letra z e o numero da matriz que se esta a tratar
            switch (op) {
                case 1:
                    double[] vecProp = CalcAHP.calculaVetorProp(matAux, lamdasMax, z);

                    if (z == 0) {
                        vecPropCriterios = vecProp;
                        //insere matriz de criterios normalizada na matriz principal
                        double[][] critNorm = matrizNormalizada(matAux, numEle);
                        int slot = nCrit + 1 + z;
                        Leitura_txt.introduzirMatrizesCalc(theMatrix, slot, critNorm, numEle, numEle);

                    } else {
                        preecheMatVecProprios(matVecProprios, vecProp, z - 1);
                        //insere matrizes normalizadas de alternativas na matriz principal
                        double[][] altNorm = matrizNormalizada(matAux, numEle);
                        int slot = nCrit + 1 + z;
                        Leitura_txt.introduzirMatrizesCalc(theMatrix, slot, altNorm, numEle, numEle);

                    }

                    //calcula RC por ordem 
                    double RC = calculoRC(lamdasMax[z], numEle);

                    vecRC[z] = RC;
                    
                    //VERIFICA SE A MATRIZ É CONSISTENTE
                    double limiteRC = Double.parseDouble(limite);
                    if (RC>limiteRC) {
                    theMatrix[w][99][99]=z;
                    w++;
                        }
                    
                    break;

                case 2:

                    //CRIAR MATRIZ NORMALIZADA
                    double[][] matNorm = matrizNormalizada(matAux, numEle);

                    vecProp = CalcAHP.VetorProprioMan(matNorm, numEle);
                    lamdasMax[z] = CalcAHP.ValorProprioMan(matAux, vecProp, numEle);

                    if (z == 0) {
                        vecPropCriterios = vecProp;
                        //insere matriz de criterios normalizada na matriz principal
                        double[][] critNorm = matrizNormalizada(matAux, numEle);
                        int slot = nCrit + 1 + z;
                        Leitura_txt.introduzirMatrizesCalc(theMatrix, slot, critNorm, numEle, numEle);

                    } else {
                        preecheMatVecProprios(matVecProprios, vecProp, z - 1);
                        //insere matrizes normalizadas de alternativas na matriz principal
                        double[][] altNorm = matrizNormalizada(matAux, numEle);
                        int slot = nCrit + 1 + z;
                        Leitura_txt.introduzirMatrizesCalc(theMatrix, slot, altNorm, numEle, numEle);

                    }

                    //calcula RC por ordem 
                    RC = calculoRC(lamdasMax[z], numEle);

                    vecRC[z] = RC;
                    
                    //VERIFICA SE A MATRIZ É CONSISTENTE
                    limiteRC = Double.parseDouble(limite);
                    if (RC>limiteRC) {
                    theMatrix[w][99][99]=z;
                    w++;
                        }
                    
                    break;

            }
        }

        //insere os vetores proprios na matriz principal
        int slot = (numMat) * 2;

        while (slot < (numMat * 3)) {

            if (slot == (numMat) * 2) {
                Leitura_txt.introduzirVetoresUnicos(theMatrix, slot, vecPropCriterios, nCrit, 0);

                slot++;
            } else {

                for (int i = 0; i < nCrit; i++) {
                    for (int j = 0; j < nAlt; j++) {
                        theMatrix[j][i][slot] = matVecProprios[j][i];

                    }
                    slot++;

                }
            }
        }

        //insere os rc's na matriz principal
        slot = (((nCrit + 1) * 3) + 1);
        Leitura_txt.introduzirVetoresUnicos(theMatrix, slot, vecRC, numMat, 0);

        //insere os valores proprios na matriz principal
        slot = ((nCrit + 1) * 3);
        Leitura_txt.introduzirVetoresUnicos(theMatrix, slot, lamdasMax, numMat, 0);

        //determina e insere vetor de prioridades compostas na matriz principal
        double[] vecPropComp = calculaVecPropComposta(matVecProprios, vecPropCriterios);

        slot = ((((nCrit + 1) * 3) + 1) + 1);
        Leitura_txt.introduzirVetoresUnicos(theMatrix, slot, vecPropComp, nAlt, 0);

        //determina e insere a posicao do valor maximo na matriz principal
        double valorMax = ValorMaxVector(vecPropComp);
        int index = 0;

        for (int i = 0; i < vecPropComp.length; i++) {
            if (vecPropComp[i] == valorMax) {
                index = i;
            }
        }
        int f = ((nCrit + 1) * 3) + 3;
        theMatrix[0][0][f] = index + 1;

    }

    //numero de colunas ou linhas duma determinada matriz quadrada
    public static int numElementos(double[][][] theMatrix, int posMatriz) {

        int numElementos = 0;

        for (int i = 0; i < theMatrix.length; i++) {

            if (theMatrix[i][0][posMatriz] != 0) {
                numElementos++;
            }

        }

        return numElementos;
    }
    
        //cria um vetor da Mat Cubica 
    //mediante a sua posicao na cubica
    public static double[] criaVecAux(double[][][] theMatrix, int nAlt, int nCrit, int posMatriz) {
        double[] vecAux = new double[nAlt];

        
            for (int j = 0; j < nAlt; j++) {
                vecAux[j] = theMatrix[j][nCrit][posMatriz];
            }
        

        return vecAux;
    }

    //cria uma matriz da Mat Cubica para o calculo do seu vector proprio
    //mediante a sua posicao na cubica
    public static double[][] criaMatAux(double[][][] theMatrix, int nAlt, int nCrit, int posMatriz) {
        double[][] matAux = new double[nAlt][nCrit];

        for (int i = 0; i < nAlt; i++) {
            for (int j = 0; j < nCrit; j++) {
                matAux[i][j] = theMatrix[i][j][posMatriz];
            }
        }

        return matAux;
    }

    //encontra valor maximo de uma matriz (lamda max)
    public static double ValorMaxMatriz(double[][] matriz) {

        double valorMax = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j] > valorMax) {
                    valorMax = matriz[i][j];
                }
            }
        }

        return valorMax;
    }

    //soma valores de uma determinada coluna de uma matriz
    public static double somaColunaMatriz(double[][] matriz, int coluna) {
        double soma = 0;

        for (int i = 0; i < matriz.length; i++) {
            soma = soma + matriz[i][coluna];
        }

        return soma;
    }

    //normaliza uma determinada coluna de uma matriz e transforma num vector
    public static double[] vecNorm(double[][] matriz, int coluna, double somaColuna) {
        double[] vec = new double[matriz.length];

        for (int i = 0; i < matriz.length; i++) {
            vec[i] = matriz[i][coluna] / somaColuna;
        }

        return vec;
    }

    //preenche uma matriz, transformando um vector num coluna, que � indicada como parametro
    public static void preecheMatVecProprios(double[][] matVecProprios, double[] vecProp, int coluna) {

        for (int i = 0; i < matVecProprios.length; i++) {
            matVecProprios[i][coluna] = vecProp[i];
        }

    }

    //determina e devolve vetor de prioridades compostas
    public static double[] calculaVecPropComposta(double[][] matriz, double[] vec) {
        double[] vecPropComp = new double[matriz.length];

        for (int i = 0; i < matriz.length; i++) {
            double soma = 0;
            for (int j = 0; j < matriz[i].length; j++) {
                soma = soma + (matriz[i][j] * vec[j]);
            }
            vecPropComp[i] = soma;
        }

        return vecPropComp;
    }

    //encontra valor maximo de um vector
    public static double ValorMaxVector(double[] vec) {
        double valorMax = 0;

        for (int i = 0; i < vec.length; i++) {
            if (vec[i] > valorMax) {
                valorMax = vec[i];
            }
        }
        return valorMax;
    }

    //calculo de Razao de consistencia
    public static double calculoRC(double lamdasMax, int numEle) {
        double RC;

        double ic = (lamdasMax - numEle) / (numEle - 1);

        RC = ic / ir[numEle - 1];

        if (RC < 0) {
            RC = RC * -1;
        }

        return RC;
    }

    //metodo retorna um vetor com a soma de cada coluna de uma matriz
    public static double[] somaColunas(double[][] matriz, int nEle) {
        double[] somaColunas = new double[nEle];
        double somaColuna;

        for (int j = 0; j < nEle; j++) {

            somaColuna = 0;

            for (int i = 0; i < nEle; i++) {
                somaColuna = somaColuna + matriz[i][j];
            }
            somaColunas[j] = somaColuna;
        }
        return somaColunas;
    }

    //m�todo de normalizacao de uma matriz
    public static double[][] matrizNormalizada(double[][] matriz, int nEle) {
        double[][] matrizNormalizada = new double[nEle][nEle];
        double[] vetorSoma = somaColunas(matriz, nEle);

        for (int i = 0; i < nEle; i++) {
            for (int j = 0; j < nEle; j++) {
                //divide cada valor de uma coluna da matriz pela soma dessa mesma coluna armazenada no vetorsoma
                matrizNormalizada[j][i] = matriz[j][i] / vetorSoma[i];
            }
        }

        return matrizNormalizada;
    }
}
