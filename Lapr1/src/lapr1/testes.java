
package lapr1;


/**
 *
 * @author manuelmonteiro
 */
public class testes {

    //vetores/matrizes/valores a serem testados
    //matriz a testar - AHP
    public final static double[][] matTeste = {{1, 0.5, 3}, {2, 1, 4}, {0.33, 0.25, 1}};
    //matriz normalizada esperada - AHP
    public final static double[][] matNormEsp = {{0.3, 0.29, 0.38}, {0.6, 0.57, 0.5}, {0.1, 0.14, 0.13}};
    //vetor proprio esperado - AHP
    public final static double[] vecPropEsp = {0.32, 0.56, 0.12};
    //valor proprio esperado - AHP
    public final static double valorPropEsp = 3.02;
    //rc esperado - AHP
    public final static double rcEsperado = 0.02;
    //Matriz inicial - TOPSIS
    public final static double[][] matriz = {{7, 9, 9, 8}, {8, 7, 8, 7}, {9, 6, 8, 9}, {6, 7, 8, 6}};
    //Vetor de pesos - TOPSIS
    public final static double[] pesos = {0.1, 0.4, 0.3, 0.2};
    //Matriz normalizada - TOPSIS
    public final static double[][] matrizNormalizadaTop = {{0.46, 0.61, 0.54, 0.53}, {0.53, 0.48, 0.48, 0.46}, {0.59, 0.41, 0.48, 0.59}, {0.40, 0.48, 0.48, 0.40}};
    //Matriz normalizada pesada - TOPSIS
    public final static double[][] matrizNormalizadaPesadaTop = {{0.05, 0.25, 0.16, 0.11}, {0.05, 0.19, 0.15, 0.09}, {0.06, 0.16, 0.15, 0.12}, {0.04, 0.19, 0.15, 0.08}};
    //Vetor Ideal Positivo - TOPSIS
    public final static double[] idealPosTop = {0.06, 0.25, 0.16, 0.08};
    //Vetor Ideal Negativo - TOPSIS
    public final static double[] idealNegTop = {0.04, 0.16, 0.15, 0.12};
    //Vetor Dist�ncia Positivo - TOPSIS
    public final static double[] distPosTop = {0.03, 0.06, 0.09, 0.06};
    //Vetor Dist�ncia Negativo - TOPSIS
    public final static double[] distNegTop = {0.09, 0.04, 0.02, 0.05};
    //Vetor Proximidade - TOPSIS
    public final static double[] proxTop = {0.75, 0.40, 0.18, 0.45};

    //Normalizar matriz e comparar com a esperada - AHP
    public static boolean test_Normalizar() {

        double[][] normalizada = MetodosComuns.matrizNormalizada(matTeste, matTeste.length);

        for (int i = 0; i < normalizada.length; i++) {
            for (int j = 0; j < normalizada.length; j++) {
                double aux = Math.round(normalizada[i][j] * 100);

                if (aux / 100 != matNormEsp[i][j]) {

                    return false;
                }

            }

        }

        return true;

    }
//Calculo Vetor Proprio e comparacao com o esperado- AHP

    public static boolean test_VetorProprioMan() {
        double[] test_vetorproprio = CalcAHP.VetorProprioMan(matNormEsp, matNormEsp.length);
        for (int i = 0; i < test_vetorproprio.length; i++) {

            double aux = Math.round(test_vetorproprio[i] * 100);

            if (aux / 100 != vecPropEsp[i]) {

                return false;
            }
        }

        return true;

    }
//Calculo Valor Proprio e comparacao com o esperado - AHP

    public static boolean test_ValorProprioMan() {
        double testMax = Math.round(CalcAHP.ValorProprioMan(matTeste, vecPropEsp, matTeste.length) * 100);

        if (testMax / 100 != valorPropEsp) {

            return false;
        }

        return true;
    }

    //Comparar RC calculado com RC esperado
    public static boolean test_Rc() {
        double testRc = Math.round(MetodosComuns.calculoRC(valorPropEsp, matTeste.length) * 100);

        if (testRc / 100 != rcEsperado) {

            return false;
        }

        return true;
    }

    //Comparar matriz normalizada TOPSIS com a esperada
    public static boolean testeNormalizarTop(double[][] matriz, int criterios, int alternativas, double[][] matrizNormalizadaTop) {

        double[][] normalizada = CalcTOPSIS.normalizador(matriz, criterios, alternativas);

        for (int i = 0; i < normalizada.length; i++) {
            for (int j = 0; j < normalizada.length; j++) {
                double aux = Math.round(normalizada[i][j] * 100);

                if (aux / 100 != matrizNormalizadaTop[i][j]) {
                    //if (Math.round(normalizada[i][j] - matrizNormalizadaTop[i][j]) * 100 > 1 || Math.round(normalizada[i][j] - matrizNormalizadaTop[i][j]) * 100 < (-1)) {

                    return false;
                }

            }

        }

        return true;

    }

    //Comparar matriz normalizada pesada TOPSIS com a esperada
    public static boolean testeNormalizarPesadaTop(double[][] matriz, double[] pesos, int criterios, int alternativas, double[][] matrizNormalizadaPesadaTop) {

        double[][] normalizadaPesada = CalcTOPSIS.matriznormpesada(matriz, pesos, criterios, alternativas);

        for (int i = 0; i < normalizadaPesada.length; i++) {
            for (int j = 0; j < normalizadaPesada.length; j++) {
                double aux = Math.round(normalizadaPesada[i][j] * 100);

                if (aux / 100 != matrizNormalizadaPesadaTop[i][j]) {
                    //if (Math.round(normalizadaPesada[i][j] - matrizNormalizadaPesadaTop[i][j]) * 100 > 1 || Math.round(normalizadaPesada[i][j] - matrizNormalizadaPesadaTop[i][j]) * 100 < (-1)) {

                    return false;
                }

            }

        }

        return true;

    }
//Comparar vetor ideal positivo com o esperado

    public static boolean testevetorIdealPositivo(double[][] matriz, int criterios, int alternativas, int custo, double[] idealPosTop) {

        double[] idealPositivo = CalcTOPSIS.idealpositivo(matriz, criterios, alternativas, custo);

        for (int i = 0; i < idealPositivo.length; i++) {
            double aux = Math.round(idealPositivo[i] * 100);

            if (aux / 100 != idealPosTop[i]) {
                //if (Math.round(idealPositivo[i] - idealPosTop[i]) * 100 > 1 || Math.round(idealPositivo[i] - idealPosTop[i]) * 100 < (-1)) {

                return false;
            }

        }

        return true;

    }
    
    //Comparar vetor ideal negativo com o esperado
    public static boolean testevetorIdealNegativo(double[][] matriz, int criterios, int alternativas, int custo, double[] idealNegTop) {

        double[] idealNegativo = CalcTOPSIS.idealnegativo(matriz, criterios, alternativas, custo);

        for (int i = 0; i < idealNegativo.length; i++) {
            double aux = Math.round(idealNegativo[i] * 100);

            if ((aux / 100) != idealNegTop[i]) {
                //if (Math.round(idealNegativo[i] - idealNegTop[i]) * 100 > 1 || Math.round(idealNegativo[i] - idealNegTop[i]) * 100 < (-1)) {

                return false;
            }

        }

        return true;

    }

    //comparar vetor distância positivo (Si*) com o esperado
    public static boolean testevetorDistanciaPositivo(double[][] matriz, int criterios, int alternativas, double[] idealMax, double[] distPosTop) {

        double[] distanciaPositivo = CalcTOPSIS.distanciaalternativa(matriz, criterios, alternativas, idealMax);

        for (int i = 0; i < distanciaPositivo.length; i++) {
            double aux = Math.round(distanciaPositivo[i] * 100);

            if (aux / 100 != distPosTop[i]) {
                //if (Math.round(distanciaPositivo[i] - distPosTop[i]) * 100 > 1 || Math.round(distanciaPositivo[i] - distPosTop[i]) * 100 < (-1)) {

                return false;
            }

        }

        return true;

    }

    //comparar vetor distância negativo (Si') com o esperado
    public static boolean testevetorDistanciaNegativo(double[][] matriz, int criterios, int alternativas, double[] idealMin, double[] distNegTop) {

        double[] distanciaNegativo = CalcTOPSIS.distanciaalternativa(matriz, criterios, alternativas, idealMin);

        for (int i = 0; i < distanciaNegativo.length; i++) {

            double aux = Math.round(distanciaNegativo[i] * 100);

            if (aux / 100 != distNegTop[i]) {
                //if (Math.round(distanciaNegativo[i] - distNegTop[i]) * 100 > 1 || Math.round(distanciaNegativo[i] - distNegTop[i]) * 100 < (-1)) {

                return false;
            }

        }

        return true;

    }

    //Comparar vetor de proximidade (Ci*) com o esperado
    public static boolean testevetorProximidade(double[] distanciapositiva, double[] distancianegativa, int alternativas, double[] proxTop) {

        double[] proximidade = CalcTOPSIS.proximidade(distanciapositiva, distancianegativa, alternativas);

        for (int i = 0; i < proximidade.length; i++) {

            double aux = Math.round(proximidade[i] * 100);

            if (aux / 100 != proxTop[i]) {
                //if (Math.round(proximidade[i] - proxTop[i]) * 100 > 1 || Math.round(proximidade[i] - proxTop[i]) * 100 < (-1)) {

                return false;
            }

        }

        return true;

    }

    public static void corretestes(double[][] matriz, double[][] matrizNormalizadaTop, double[] pesos, double[][] matrizNormalizadaPesadaTop, double[] idealPosTop, double[] idealNegTop, double[] idealMax, double[] idealMin, double[] distPosTop, double[] distNegTop, double[] distanciapositiva, double[] distancianegativa, double[] proxTop) {

        System.out.println(test_Normalizar() ? "Matriz Normalizada AHP: Ok" : "Matriz Normalizada AHP: Not Ok");

        System.out.println(test_VetorProprioMan() ? "Vetor Proprio AHP: Ok" : "Vetor Proprio AHP: Not Ok");

        System.out.println(test_ValorProprioMan() ? "Valor Proprio AHP: Ok" : "Valor Proprio AHP: Not Ok");

        System.out.println(test_Rc() ? "RC AHP: Ok" : "RC AHP: Not Ok");

        System.out.println(testeNormalizarTop(matriz, 4, 4, matrizNormalizadaTop) ? "Matriz Normalizada TOPSIS: Ok" : "Matriz Normalizada TOPSIS: Not Ok");

        System.out.println(testeNormalizarPesadaTop(CalcTOPSIS.normalizador(matriz, 4, 4), pesos, 4, 4, matrizNormalizadaPesadaTop) ? "Matriz Normalizada Pesada TOPSIS: Ok" : "Matriz Normalizada Pesada TOPSIS: Not Ok");

        System.out.println(testevetorIdealPositivo(CalcTOPSIS.matriznormpesada(CalcTOPSIS.normalizador(matriz, 4, 4), pesos, 4, 4), 4, 4, 1, idealPosTop) ? "Vetor Ideal Positivo TOPSIS: Ok" : "Vetor Ideal Positivo TOPSIS: Not Ok");

        System.out.println(testevetorIdealNegativo(CalcTOPSIS.matriznormpesada(CalcTOPSIS.normalizador(matriz, 4, 4), pesos, 4, 4), 4, 4, 1, idealNegTop) ? "Vetor Ideal Negativo TOPSIS: Ok" : "Vetor Ideal Negativo TOPSIS: Not Ok");

        System.out.println(testevetorDistanciaPositivo(CalcTOPSIS.matriznormpesada(CalcTOPSIS.normalizador(matriz, 4, 4), pesos, 4, 4), 4, 4, CalcTOPSIS.idealpositivo(CalcTOPSIS.matriznormpesada(CalcTOPSIS.normalizador(matriz, 4, 4), pesos, 4, 4), 4, 4, 1), distPosTop) ? "Vetor Distância Positivo TOPSIS: Ok" : "Vetor Distância Positivo TOPSIS: Not Ok");

        System.out.println(testevetorDistanciaNegativo(CalcTOPSIS.matriznormpesada(CalcTOPSIS.normalizador(matriz, 4, 4), pesos, 4, 4), 4, 4, CalcTOPSIS.idealnegativo(CalcTOPSIS.matriznormpesada(CalcTOPSIS.normalizador(matriz, 4, 4), pesos, 4, 4), 4, 4, 1), distNegTop) ? "Vetor Distância Negativo TOPSIS: Ok" : "Vetor Distância Negativo TOPSIS: Not Ok");

        System.out.println(testevetorProximidade(distanciapositiva, distancianegativa, 4, proxTop) ? "Vetor Proximidade TOPSIS: Ok" : "Vetor Proximidade TOPSIS: Not Ok");

    }

    public static void main(String[] args) {

        corretestes(matriz, matrizNormalizadaTop, pesos, matrizNormalizadaPesadaTop, idealPosTop, idealNegTop, idealPosTop, idealPosTop, distPosTop, distNegTop, distPosTop, distNegTop, proxTop);
    }

}
